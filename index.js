/** @format */

class NilaiMhs {
  constructor() {
    this.input;
    this.listNilaiMhs = [];

    console.log('=========================================================');
    console.log('\t\tProgram Nilai Siswa');
    console.log('=========================================================');
    console.log('Masukkan nilai siswa dan ketik "q" jika sudah selesai');
  }
  inputNilai() {
    const readline = require('readline');
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });

    const question = (question) => {
      return new Promise((resolve) => {
        rl.question(question, (answer) => {
          resolve(answer);
        });
      });
    };

    const isiNilai = async () => {
      while (this.input !== 'q') {
        if (this.input === 'q') {
          break;
        }

        this.input = await question('Masukkan nilai: ');
        this.listNilaiMhs.push(this.input);
      }
      this.listNilaiMhs.pop();
      console.log(`Nilai Terendah : ${this.nilaiTerendah()}`);
      console.log(`Nilai Tertinggi : ${this.nilaiTerTinggi()}`);
      console.log(`Rata-rata : ${this.rataRata()}`);
      console.log(`Jumlah Siswa Lulus : ${this.jumlahSiswaLulus()}`);
      console.log(`Jumlah Siswa Tidak Lulus : ${this.jumlahSiswaTidakLulus()}`);
      console.log(`Urutan Nilai : ${this.urutanNilaiBubbleSort()}`);
      rl.close();
    };
    return isiNilai();
  }

  // convert listNilaiMhs string to number
  convertNilai() {
    let result = this.listNilaiMhs.map((nilaiMhs) => Number(nilaiMhs));
    return result;
  }

  // Nilai Terendah
  nilaiTerendah() {
    let nilaiTerendah = this.convertNilai()[0];
    for (let i = 0; i < this.convertNilai().length; i++) {
      if (nilaiTerendah > this.convertNilai()[i]) {
        nilaiTerendah = this.convertNilai()[i];
      }
    }
    // console.log(`nilai terendah : ${nilaiTerendah}`);
    return nilaiTerendah;
  }

  // Nilai Tertinggi
  nilaiTerTinggi() {
    let nilaiTerTinggi = this.convertNilai()[0];
    for (let i = 0; i < this.convertNilai().length; i++) {
      if (nilaiTerTinggi < this.convertNilai()[i]) {
        nilaiTerTinggi = this.convertNilai()[i];
      }
    }
    // console.log(`nilai tertinggi : ${nilaiTerTinggi}`);
    return nilaiTerTinggi;
  }

  // Rata-rata
  rataRata() {
    let rataRata = 0;
    for (let i = 0; i < this.convertNilai().length; i++) {
      rataRata += this.convertNilai()[i];
    }
    // console.log(`rata-rata : ${Math.round((rataRata / this.convertNilai().length) * 100) / 100}`);
    return Math.round((rataRata / this.convertNilai().length) * 100) / 100;
  }

  // Jumlah Siswa Lulus
  jumlahSiswaLulus() {
    let jumlahSiswaLulus = 0;
    for (let i = 0; i < this.convertNilai().length; i++) {
      if (this.convertNilai()[i] >= 75) {
        jumlahSiswaLulus++;
      }
    }
    // console.log(`jumlah siswa lulus : ${jumlahSiswaLulus}`);
    return jumlahSiswaLulus;
  }

  // Jumlah Siswa Tidak Lulus
  jumlahSiswaTidakLulus() {
    let jumlahSiswaTidakLulus = 0;
    for (let i = 0; i < this.convertNilai().length; i++) {
      if (this.convertNilai()[i] < 75) {
        jumlahSiswaTidakLulus++;
      }
    }
    // console.log(`jumlah siswa tidak lulus : ${jumlahSiswaTidakLulus}`);
    return jumlahSiswaTidakLulus;
  }

  // Nilai Terendah ke Tertinggi dengan bubble sort
  urutanNilaiBubbleSort() {
    let nilaiTerendahKeTertinggi = this.convertNilai();
    for (let i = 0; i < nilaiTerendahKeTertinggi.length; i++) {
      for (let j = 0; j < nilaiTerendahKeTertinggi.length - 1; j++) {
        if (nilaiTerendahKeTertinggi[j] > nilaiTerendahKeTertinggi[j + 1]) {
          let temp = nilaiTerendahKeTertinggi[j];
          nilaiTerendahKeTertinggi[j] = nilaiTerendahKeTertinggi[j + 1];
          nilaiTerendahKeTertinggi[j + 1] = temp;
        }
      }
    }
    // console.log(`nilai terendah ke tertinggi : ${nilaiTerendahKeTertinggi}`);
    return nilaiTerendahKeTertinggi;
  }
}

const nilaiMhs = new NilaiMhs();
nilaiMhs.inputNilai();
